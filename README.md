
# Low SWaP Lab icons

Various icons


# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/icons](https://www.npmjs.com/package/@lowswaplab/icons)

This package can be installed by performing:

    npm install @lowswaplab/icons


# Source Code

[icons](https://gitlab.com/lowswaplab/icons)


# Icons

![](src/aircraft.svg "Aircraft")
![](src/aircraft-helicopter.svg "Helicopter")
![](src/aircraft-sar.svg "SAR Aircraft")
![](src/aircraft-sar-helicopter.svg "SAR Helicopter")
![](src/airport.svg "Airport")
![](src/airport-closed.svg "Airport")
![](src/ais-station.svg "AIS Station")
![](src/antenna-dish.svg "Antenna Dish")
![](src/arrow.svg "Arrow")
![](src/aton.svg "Aid to Navigation")
![](src/aton-virtual.svg "Virtual Aid to Navigation")
![](src/circle.svg "Circle")
![](src/heliport.svg "Heliport")
![](src/lsl-propellor.svg "LSL Propellor")
![](src/sart.svg "Search And Rescue Transponder")
![](src/satellite.svg "Satellite")
![](src/vehicle-car.svg "Car")
![](src/vessel-a.svg "Vessel, Class A")
![](src/vessel-a-5kn.svg "Vessel, Class A, ≥ 5kn")
![](src/vessel-a-10kn.svg "Vessel, Class A, ≥ 10kn")
![](src/vessel-a-15kn.svg "Vessel, Class A, ≥ 15kn")
![](src/vessel-a-20kn.svg "Vessel, Class A, ≥ 20kn")
![](src/vessel-b.svg "Vessel, Class B")
![](src/vessel-b-5kn.svg "Vessel, Class B, ≥ 5kn")
![](src/vessel-b-10kn.svg "Vessel, Class B, ≥ 10kn")
![](src/vessel-b-15kn.svg "Vessel, Class B, ≥ 15kn")
![](src/vessel-b-20kn.svg "Vessel, Class B, ≥ 20kn")
![](src/vessel-b-sailboat.svg "Sailboat, Class B")
![](src/vessel-b-sailboat-5kn.svg "Sailboat, Class B, ≥ 5kn")
![](src/vessel-b-sailboat-10kn.svg "Sailboat, Class B, ≥ 10kn")
![](src/vessel-b-sailboat-15kn.svg "Sailboat, Class B, ≥ 15kn")
![](src/vessel-b-sailboat-20kn.svg "Sailboat, Class B, ≥ 20kn")
![](src/vessel-tug.svg "Tug")
![](src/vessel-tug-5kn.svg "Tug, ≥ 5kn")
![](src/vessel-tug-10kn.svg "Tug, ≥ 10kn")
![](src/vessel-tug-15kn.svg "Tug, ≥ 15kn")
![](src/vessel-tug-20kn.svg "Tug, ≥ 20kn")
![](src/vessel-vor.svg "VOR")
![](src/vessel-vor-dme.svg "VOR-DME")
![](src/vessel-vortac.svg "VORTAC")


# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).


# Author

[Low SWaP Lab](https://www.lowswaplab.com/)


# Copyright

Copyright © 2021-2022 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)


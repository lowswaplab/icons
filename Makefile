
# Low SWaP Lab Icons

# https://gitlab.com/lowswaplab/icons

# Copyright © 2021-2022 Low SWaP Lab lowswaplab.com

INSTALL_PREFIX = /usr/local

EXIFTOOL_FLAGS := \
  -all= \
  -artist="Low SWaP Lab" \
  -copyright="© 2021-2022 Low SWaP Lab lowswaplab.com" \
  -overwrite_original

SCOUR_FLAGS := \
  -q \
  --enable-viewboxing \
  --no-line-breaks \
  --strip-xml-space \
  --error-on-flowtext

PNGS := \
  dist/aircraft.png \
  dist/aircraft-helicopter.png \
  dist/aircraft-sar.png \
  dist/aircraft-sar-helicopter.png \
  dist/airport.png \
  dist/airport-closed.png \
  dist/ais-station.png \
  dist/antenna-dish.png \
  dist/antenna-lpda.png \
  dist/arrow.png \
  dist/aton.png \
  dist/aton-virtual.png \
  dist/camera.png \
  dist/circle.png \
  dist/heliport.png \
  dist/lsl-propellor.png \
  dist/sart.png \
  dist/satellite.png \
  dist/vehicle-car.png \
  dist/vessel-a.png \
  dist/vessel-a-5kn.png \
  dist/vessel-a-10kn.png \
  dist/vessel-a-15kn.png \
  dist/vessel-a-20kn.png \
  dist/vessel-b.png \
  dist/vessel-b-5kn.png \
  dist/vessel-b-10kn.png \
  dist/vessel-b-15kn.png \
  dist/vessel-b-20kn.png \
  dist/vessel-b-sailboat.png \
  dist/vessel-b-sailboat-5kn.png \
  dist/vessel-b-sailboat-10kn.png \
  dist/vessel-b-sailboat-15kn.png \
  dist/vessel-b-sailboat-20kn.png \
  dist/vessel-tug.png \
  dist/vessel-tug-5kn.png \
  dist/vessel-tug-10kn.png \
  dist/vessel-tug-15kn.png \
  dist/vessel-tug-20kn.png \
  dist/vor.png \
  dist/vor-dme.png \
  dist/vortac.png

# icons to rotate
ROTATE := \
  dist/aircraft.png \
  dist/aircraft-helicopter.png \
  dist/antenna-dish.png \
  dist/antenna-lpda.png \
  dist/arrow.png \
  dist/camera.png \
  dist/satellite.png \
  dist/vehicle-car.png \
  dist/vessel-a.png \
  dist/vessel-a-5kn.png \
  dist/vessel-a-10kn.png \
  dist/vessel-a-15kn.png \
  dist/vessel-a-20kn.png \
  dist/vessel-b.png \
  dist/vessel-b-5kn.png \
  dist/vessel-b-10kn.png \
  dist/vessel-b-15kn.png \
  dist/vessel-b-20kn.png \
  dist/vessel-b-sailboat.png \
  dist/vessel-b-sailboat-5kn.png \
  dist/vessel-b-sailboat-10kn.png \
  dist/vessel-b-sailboat-15kn.png \
  dist/vessel-b-sailboat-20kn.png \
  dist/vessel-tug.png \
  dist/vessel-tug-5kn.png \
  dist/vessel-tug-10kn.png \
  dist/vessel-tug-15kn.png \
  dist/vessel-tug-20kn.png

VERSION = `./version`

.PHONY: all
all:	dist $(PNGS) rotate zip Makefile

install:	clean all
		mkdir -p $(INSTALL_PREFIX)/share/icons/lowswaplab
		cp dist/* $(INSTALL_PREFIX)/share/icons/lowswaplab
		chmod -R a+rX $(INSTALL_PREFIX)/share/icons/lowswaplab

dist:
		mkdir dist

.PRECIOUS: $(PNGS:.png=.svg)
dist/%.svg:		src/%.svg Makefile
	scour $(SCOUR_FLAGS) -i "$(<)" -o "$(@)"

dist/%.png:		dist/%.svg Makefile
	convert -background none -geometry 32x32 "$(<)" "$(@)"
	exiftool $(EXIFTOOL_FLAGS) "$(@)"

rotate:
	for file in $(ROTATE); \
    do \
	  for rot in 000 015 030 045 060 075 090 105 120 135 150 165 180 \
	    195 210 225 240 255 270 285 300 315 330 345; \
	  do \
	    OUT=`basename $$file .png`; \
        convert "$$file" -background none -gravity center -rotate $$rot \
	      -trim -extent 32x32 +repage "dist/$$OUT-$$rot.png"; \
	  done \
	done

#        convert "$$file" \( +clone -background none -rotate $$rot \) \
#	      -gravity center -compose Src -composite \
#	      "dist/$$OUT-$$rot.png"; \
#        convert "$$file" -background none -gravity center -rotate $$rot \
#	      +repage "dist/$$OUT-$$rot.png"; \

zip:	LowSWaPLab-iconset-$(VERSION).zip

# In iconset.xml, the v5 UUID is based on the DNS namespace
# (6ba7b810-9dad-11d1-80b4-00c04fd430c8) with a string of "lowswaplab.com":
# f631b5dc-af4d-5eed-9abc-b7c9066082c4
LowSWaPLab-iconset-$(VERSION).zip:	dist
		zip -qj LowSWaPLab-iconset-$(VERSION).zip README.md iconset.xml \
		  dist/*.png

npm-publish:	clean all
#	npm login
	npm publish --access public

.PHONY: clean
clean:
		$(RM) dist/* *.zip

